import {REQUEST_INSTAGRAM, REQUEST_LOCAL, RECEIVE_INSTAGRAM, RECEIVE_LOCAL, HANDLE_MODAL_WINDOW, ACCESS_CAMERA, PHOTO_SAVED} from '../actions/action.js'

const initialState = {
    test : "",
    loading: true,
    instagramData : {},
    modalWindow: false,
    src: "",
    imgSrc: "",
    photo: "",
    source_type: "0"
};

function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case REQUEST_INSTAGRAM :
            return Object.assign({},state,{
                loading: action.loading
            });
        case REQUEST_LOCAL :
            return Object.assign({},state,{
                loading: action.loading
            });
        case RECEIVE_INSTAGRAM :
            return Object.assign({},state,{
                instagramData : action.meta,
                loading : action.loading,
                source_type: "0"
            });
        case RECEIVE_LOCAL:
            console.log(action.meta);
            return Object.assign({},state,{
                instagramData : action.meta,
                loading : action.loading,
                source_type: "1"
            });
        case HANDLE_MODAL_WINDOW :
            return Object.assign({},state,{
                modalWindow : action.modalWindow,
                imgSrc: action.imgSrc
            });
        case ACCESS_CAMERA :
            return Object.assign({},state,{
                src: action.src
            });
        case PHOTO_SAVED :
            return Object.assign({}, state, {
                photo: action.photo
            });
        default :
            return state;
    }
}

export default reducer;