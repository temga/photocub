import fetch from 'isomorphic-fetch';

export const REQUEST_INSTAGRAM   = 'REQUEST_INSTAGRAM';
export const REQUEST_LOCAL       = 'REQUEST_LOCAL';
export const RECEIVE_INSTAGRAM   = 'RECEIVE_INSTAGRAM';
export const RECEIVE_LOCAL       = 'RECEIVE_LOCAL';
export const HANDLE_MODAL_WINDOW = 'HANDLE_WINDOW';
export const ACCESS_CAMERA       = 'ACCESS_CAMERA';
export const PHOTO_SAVED         = 'PHOTO_SAVED';
export const CHANGE_SOURCE       = 'CHANGE_SOURCE';
export const CHANGE_TYPE         = 'CHANGE_TYPE';

export function requestInstagram(loading) {
    return {
        type : REQUEST_INSTAGRAM,
        loading
    }
}

export function requestLocal(loading) {
    return {
        type : REQUEST_LOCAL,
        loading
    }
}

function receiveInstagram(loading,json) {
    return {
        type  : RECEIVE_INSTAGRAM,
        loading : loading,
        meta  : json
    }
}

function receiveLocal(loading,json) {
    return {
        type  : RECEIVE_LOCAL,
        loading : loading,
        meta  : json
    }
}

export function fetchInstagram() {
    return dispatch => {
        dispatch(requestInstagram(true));
        return fetch('/insta')
            .then(response => response.json())
            .then(json => dispatch(receiveInstagram(false,json)))
    }
}

export function fetchLocal() {
    return dispatch => {
        dispatch(requestLocal(true));
        return fetch('/getphoto')
            .then(response => response.json())
            .then(json => dispatch(receiveLocal(false,json)))
    }
}

export function handleWindow(modalWindow, imgSrc = null) {
    return {
        type : HANDLE_MODAL_WINDOW,
        modalWindow,
        imgSrc
    }
}

export function accessCamera(src) {

    return {
        type: ACCESS_CAMERA,
        src: src
    }
}

export function connectCamera() {
    return dispatch => {
        navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia;
        navigator.getUserMedia({video: true}, (stream) => {
            let src = window.URL.createObjectURL(stream);
            dispatch(accessCamera(src));
        }, (err) => console.log(err));
    }
}

export function sendPhoto(photo) {
    return dispatch => {
        fetch('/getphoto',{
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(photo)
        })
        .then(response => {
                dispatch(changeSource("1"));
                dispatch(handleWindow(false));
                //response.toJSON()
            })
        .then(json => {
                dispatch()
            })
    }
}

export function photoSaved(photo) {
    return {
        type: PHOTO_SAVED,
        photo
    }
}


export function changeSource(source) {
    return dispatch => {
        switch (source) {
            case "0":
                dispatch(fetchInstagram());
            case "1":
                dispatch(fetchLocal());
        }
    }
}