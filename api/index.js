var router = require('express').Router();
var path  = require('path');
import {config} from '../config.js';
import {getMetaTags} from './instagram.js'
import {photoModel} from '../db/db.js'


/*
Get Instagram metaData
 */
router.get('/insta',function(req, res){
    getMetaTags().then((json) => {
        res.send(json)
    });
});

/*
Отправляем картинки
 */

router.get('/getphoto', (req, res) => {
    photoModel.find().sort({'_id': 'desc'}).exec((err, photo) => {
        res.send({ minInfo : photo });
    });
});

/*
Сохраняем картинку
 */
router.post('/getphoto', (req, res) => {
    if (req.body.photo != "undefined") {
        let newPhoto = new photoModel({
            username: 'local',
            url: req.body.photo,
            tag:  config.insta_api.tag
        });
        newPhoto.save((err)=> {
            if (err) return res.send({status:502});
            res.send({status:200});
        });
    }
    else {
        console.log(req.body);
        res.send({status: 502});
    }
});

router.get('/',function(req, res){
    res.sendFile(path.join(__dirname,'../index.html'));
});

module.exports = router;

console.info('==> Api start...');

