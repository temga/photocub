/**
 * Created by artemorlov on 15.10.15.
 */
import {config} from '../config.js'
require('es6-promise').polyfill();
require('isomorphic-fetch');


export function getMetaTags(tag = config.insta_api.tag, client_id = config.insta_api.CLIENT_ID) {

    return fetch(`https://api.instagram.com/v1/tags/${tag}/media/recent?client_id=${client_id}`)
        .then(function(response) {
            return response.json()
        }).then(function(json) {
            return { minInfo : getMinInfo(json) }
        }).catch(function(ex) {
            return ex
        })
}

function getMinInfo(json){
    let min = [];
    const data = json.data;
    for (let i = 0;i<=18;i++) {
        let minInfo = {};
        if (data[i].type == "image") {
        minInfo = {
            username : data[i].user.username,
            url : data[i].images.standard_resolution.url
        };
        min[i] = minInfo
        }
    }
    return min
}


