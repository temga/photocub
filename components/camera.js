import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'

class Webcam extends Component {

    constructor(props) {
        super(props);
    }

    render(){
        console.log(this.props);
        return(
            <video src={this.props.src}  className={this.props.className} autoPlay>Hello</video>
        );
    }
}

function select(state) {
    return {
        src: state.src
    }
}

export default connect(select)(Webcam)