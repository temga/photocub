/**
 * Created by artemorlov on 14.10.15.
 */
import React, { Component, PropTypes } from 'react';
import {config} from '../config.js'
import { connect } from 'react-redux';
import { changeSource } from '../actions/action.js'

let Fotter = React.createClass( {

    _handleChange(e) {
        this.props.dispatch(changeSource(e.target.value));
    },

    render() {
        return(
            <div className="photocab footer">
                <span className="tag">#{config.insta_api.tag}</span>
                <select value={this.props.type} onChange={this._handleChange}>
                    <option value="0">Instagram</option>
                    <option value="1"> Local</option>
                </select>
            </div>
        );
    }
});

function select(state) {
    return {
        source_type: state.source_type
    }
}

export default connect(select)(Fotter)