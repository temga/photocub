import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'
import { handleWindow, connectCamera } from '../actions/action.js'

let Header = React.createClass ({

    _handleClick()  {
      console.log(this.props);
      this.props.dispatch(handleWindow(true))
    },

    componentWillMount() {
        this.props.dispatch(connectCamera());
    },

    render() {
        return(
        <div className="photocab header">
            <div className="logo"/>
            <div className="photo" onClick={this._handleClick}/>
        </div>
    );
}
});

function select(state) {
    return {
        test : state.test,
        insta: state.insta
    }
}

export default connect(select)(Header)