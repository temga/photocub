import React from 'react'
import Modal from 'react-modal'
import TakePhoto from './modal_takephoto.js'
import { connect } from 'react-redux';
import { handleWindow, sendPhoto } from '../actions/action.js'

const customStyle = {
    content : {
        padding : 0
    }
};

let ModalWindow = React.createClass({

    _onClose() {
        this.props.dispatch(handleWindow(false));
    },

    render() {
        return(
            <Modal
            isOpen={this.props.modalWindow}
            onRequestClose={this._onClose}
            style = {customStyle}>
                <TakePhoto/>
            </Modal>
        );
    }
});

function select(state) {
    return {
        modalWindow : state.modalWindow
    }
}

export default connect(select)(ModalWindow)