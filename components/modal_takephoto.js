import React, { Component, PropTypes } from 'react'
import Webcam from './camera.js'
import { sendPhoto } from '../actions/action.js'

class TakePhoto extends Component {

    constructor(props) {
        super(props);
        this._onPhoto= this._onPhoto.bind(this);
    }

    _renderPhoto() {
        if (!this.props.imgSrc) {
            return(
            <div>
            <Webcam className="image-to-capture"/>
            <div className="row">
                <button onClick={this._onPhoto}>СДЕЛАТЬ ФОТО!</button>
                </div>
            </div>
            );
        }
        else {
            console.log("here");
            return(
                <div className="modal-image" style={{backgroundImage:`url(${this.props.imgSrc})`}}/>
            );
        }
    }

    _onPhoto() {
        let canvas = document.createElement('canvas');
        canvas.width = 640;
        canvas.height = 480;
        let ctx = canvas.getContext('2d');
        let video = document.getElementsByClassName("image-to-capture");
        ctx.drawImage(video[0], 0, 0, canvas.width, canvas.height);

        var dataURI = canvas.toDataURL('image/jpeg');
        console.log(dataURI);
        this.props.dispatch(sendPhoto({photo:dataURI}));

    }

    render() {
        return(
            <div className="take-photo">
                {this._renderPhoto()}
            </div>
        );
    }
}

function select(state) {
    return {
        imgSrc : state.imgSrc
    }
}

import { connect } from 'react-redux';
export default connect(select)(TakePhoto);