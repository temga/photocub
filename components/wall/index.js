/**
 * Created by artemorlov on 14.10.15.
 */
import React, { Component, PropTypes } from 'react';
import Page from './page.js';
import {connect} from 'react-redux';
import {fetchInstagram, fetchLocal} from '../../actions/action.js'

var Slider = require('react-slick');

let Wall = React.createClass ({

    getCaruselPages() {
        let pages = [];
        for (let i=0;i<=2;i++){
          pages.push(<div key={i} className="photo-page"><Page number={i} data={this.props.instagramData} loading={this.props.loading}/></div>);
        }
        return pages;
    },

    componentWillMount() {
        console.log(this.props.source_type);
        switch (this.props.source_type) {
            case "0":
                this.props.dispatch(fetchInstagram());
            case "1":
                this.props.dispatch(fetchLocal());
        }
        //setInterval(() => this.props.dispatch(fetchInstagram()),10000);
    },

    render() {
        var settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        return(
            <div className="photocab wall">
                <div className="photo-carusel">
                    <Slider {...settings}>
                        {this.getCaruselPages()}
                    </Slider>
                </div>
            </div>
        );
    }
});

function select(state){
    return {
        instagramData:state.instagramData,
        loading: state.loading,
        source_type : state.source_type
    }
}

export default connect(select)(Wall)