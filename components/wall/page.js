/**
 * Created by artemorlov on 14.10.15.
 */
import React, { Component, PropTypes } from 'react';
import Photo from './photo.js'


let Page = React.createClass({

    getPhotos(pag = 2,uniq = 0) {
        let photos = [];
        for (let i=0;i<=pag;i++) {
            const index = 6*this.props.number + (3*uniq) + i - 1;
            const cam = ((i == 0 && uniq == 0) && (this.props.number == 0));
            photos.push(<Photo key={index} cam={cam} loading={this.props.loading} data={(this.props.data.minInfo != undefined && !cam) ? this.props.data.minInfo[index] : {}}/>);
        }
        return photos;
    },

    render() {
        return(
            <div>
                <div className="photo-set">
                    {this.getPhotos(2,0)}
                </div>
                <div className="photo-set">
                    {this.getPhotos(2,1)}
                </div>
            </div>
        )
    }
});

export default Page