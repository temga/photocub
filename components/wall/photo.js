/**
 * Created by artemorlov on 14.10.15.
 */
import React, { Component, PropTypes } from 'react'
import { handleWindow } from '../../actions/action.js'
import { connect } from 'react-redux'
import Webcam from '../camera.js'
import Loader from 'react-loaders'

let Photo = React.createClass({

    _handleClick() {
        this.props.dispatch(handleWindow(true, this.props.data.url))
    },

    _getPhotoBody() {
        if (this.props.cam) {
            if (this.props.modalWindow) {
                return (<span/>)
            }
            else {
                return (
                    <div className="basic-image">
                      <Webcam/>
                    </div>
                );
            }
        }
        else {
            if (this.props.loading) {
                return (
                    <Loader type="line-scale" active="true"/>
                );
            }
            else {
                if (this.props.data) {
                    return (
                        <div className="basic-image" style={{backgroundImage:`url(${this.props.data.url})`}}/>
                    );
                }
                else {
                    return (<div className="logo"/>);
                }
            }
        }
    },

    render() {
        //console.log(this.props);
        let user_name = "Take a photo!!!!";
        if (!this.props.cam && this.props.data) {
            user_name = this.props.data.username
        }
        return (
            <div className="photo-desc" onClick={this._handleClick}>
                {this._getPhotoBody()}
                <span className="photo-label">{user_name}</span>
            </div>
        )
    }
});

function select(state) {
    return {
        modalWindow: state.modalWindow,
        imgSrc: state.imgSrc
    }
}

export default connect(select)(Photo)