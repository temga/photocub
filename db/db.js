let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/photocap');

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (cb) {
    console.log("==> Connected to db...");
});

let photoSchema = mongoose.Schema({
    username: String,
    url     : String,
    tag     : String
});

export let photoModel = mongoose.model('Photo', photoSchema);