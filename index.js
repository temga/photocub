import 'babel-core/polyfill';
import React from 'react';
import { render } from 'react-dom';
import Header from './components/header.js'
import Footer from './components/footer.js'
import Wall from './components/wall/index.js'
import { Provider, connect } from 'react-redux';
import configureStore from './store/store.js'
import ModalWindow from './components/modal.js'
require('./style/style.scss');

const store = configureStore();

render(
	<Provider store={store}>
		<div className="main-content-body">
			<Header/>
			<Wall/>
			<Footer/>
			<ModalWindow/>
		</div>
	</Provider>,
	document.getElementById('root')
	);

