/**
 * Created by artemorlov on 22.10.15.
 */
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../Reducers/reducers.js';

const createStoreWithMiddleware = applyMiddleware(
    thunkMiddleware
)(createStore);

export default function configureStore(initialState) {
    const store = createStoreWithMiddleware(rootReducer, initialState);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../Reducers/reducers.js', () => {
            const nextRootReducer = require('../Reducers/reducers.js');
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}